import { configure, setAddon, addDecorator } from "@storybook/react";
import LiveEdit, { setOptions } from "storybook-addon-react-live-edit";


// React-live-edit
setOptions({ theme: "darcula", presets: ["react"], enableShortcuts: false });
setAddon(LiveEdit);

// automatically import all files ending in *.stories.js
const req = require.context("../src", true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
