import { createMuiTheme } from "@material-ui/core/styles";

const MuiTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4086ff",
      dark: "#005ACB"
    },
    secondary: {
      main: "#E67381",
      dark: "#D0021B"
    }
  },
  typography: {
    h2: {
      color: "#212121",
      fontSize: 28,
      fontWeight: 600,
      lineHeight: "45px"
    },
    body1: {
      color: "#9b9b9b",
      fontSize: 24,
      lineHeight: "30px"
    }
  }
});

export default MuiTheme;
