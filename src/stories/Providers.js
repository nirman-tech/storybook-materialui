import React from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/styles";

const ProviderWrapper = ({ children, theme }) => <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;

export default ProviderWrapper;
