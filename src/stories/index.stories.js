import React from "react";
import { addDecorator } from "@storybook/react";

import ProviderWrapper from "./Providers";
import MuiTheme from "../assets/MuiTheme";

const withProviders = story => <ProviderWrapper theme={MuiTheme}>{story()}</ProviderWrapper>;

addDecorator(withProviders);
