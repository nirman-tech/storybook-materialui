import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import CardHeader from "@material-ui/core/CardHeader";
import Card from "@material-ui/core/Card";
import Modal from "@material-ui/core/Modal";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

import { useStyles } from "./styles";

export const Loader = ({ isLoading, title, content }) => {
  const classes = useStyles();

  return (
    <Modal disableBackdropClick open={isLoading}>
      <Card className={classes.card}>
        <CardHeader title={<Typography variant="h2">{title}</Typography>} className={classes.cardHeader} />
        <Divider />
        <CircularProgress className={classes.circularProgress} />
        <CardContent>
          <Typography variant="body1" component="p">
            {content}
          </Typography>
        </CardContent>
      </Card>
    </Modal>
  );
};

export default Loader;
