import React from "react";
import { storiesOf } from "@storybook/react";
import Loader from "./";
import withLiveEditScope from "storybook-addon-react-live-edit/dist/withLiveEditScope";

storiesOf("AppLoader", module)
.addParameters({
    options: {
        enableShortcuts: false,
      },
})
.addDecorator(withLiveEditScope({ React, Loader }))
.addLiveSource("live demo", `return <Loader isLoading title="Sample Header" content="Please wait. this is a sample loader text" />`);
