import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: "200px auto 0",
    textAlign: "center"
  },
  circularProgress: { display: "inline-block", marginTop: 12 }
}));
