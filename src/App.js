import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import MuiTheme from "./assets/MuiTheme";

function App() {
  return (
    <div className="App">
      <MuiThemeProvider theme={MuiTheme}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </a>
        </header>
      </MuiThemeProvider>
    </div>
  );
}

export default App;
